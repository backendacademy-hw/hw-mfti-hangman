# Игра "Виселица"

## Описание
Вашему вниманию представляется вариант классической игры "Виселица" для командной строки.
Всегда загадывается слова составленное из латинских букв нижнего регистра.
Вводить альтеранативные символы бесполезно.

## Примеры взаимодействия
Пример выигрыша

```
> The word: *****
>
> Guess a letter:
< a
> Missed, mistake 1 out of 5.
>
> The word: *****
>
> Guess a letter:
< b
> Missed, mistake 2 out of 5.
>
> The word: *****
>
> Guess a letter:
< e
> Hit!
>
> The word: *e***
>
> Guess a letter:
< o
> Hit!
>
> The word: *e**o
>
> Guess a letter:
< l
> Hit!
>
> The word: *ello
>
> Guess a letter:
< h
> Hit!
>
> The word: hello
>
> You won!
```

Пример проигрыша
```
> The word: ******
>
> Guess a letter:
< x
> Missed, mistake 1 out of 5.
>
> The word: ******
>
> Guess a letter:
< y
> Missed, mistake 2 out of 5.
>
> The word: ******
>
> Guess a letter:
< z
> Missed, mistake 3 out of 5.
>
> The word: ******
>
> Guess a letter:
< n
> Hit!
>
> The word: **n*n*
>
> Guess a letter:
< m
> Missed, mistake 4 out of 5.
>
> The word: **n*n*
>
> Guess a letter:
< o
> Missed, mistake 5 out of 5.
>
> The word: **n*n*
>
> You lost!
```

## Руководство по работе с проектом
Для запуска игры достаточно в терминале перейти в папку с игрой и ввести команду:
```
> sbt run
```

При работе с проектом, вам может понадобиться запусе юнит-тестов. Он осуществляется командой:
```
> sbt test
```

Для просмотра покрытия кода тестами, введите команду:
```
> sbt clean coverage test coverageReport
```
и следуйте указаниям в терминале для просмотра coverage-report.