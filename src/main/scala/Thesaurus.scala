import scala.util.Random

trait Thesaurus {
  def randomWord: String
}

case class MyThesaurus() extends Thesaurus {
  val words: Vector[String] = Vector(
    "actor",
    "beach",
    "hello",
    "physics",
    "math",
    "literature",
    "cinema",
    "music",
    "rock",
    "scala",
    "mist",
    "park",
    "archeology"
  )

  override def randomWord: String = words(Random.nextInt(words.length))
}
