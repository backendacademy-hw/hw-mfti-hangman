case class Hangman(hiddenWord: String, guessedLetters: String, mistakes: Int)

case class Playground(dict: Thesaurus) {
  val maxMistakes = 5

  def start: Unit = {
    val hiddenWord             = dict.randomWord
    val guessedLetters: String = "*" * hiddenWord.length
    val game                   = Hangman(hiddenWord, guessedLetters, 0)
    checkStateAndGo(game)
  }

  def checkStateAndGo(game: Hangman): Unit = {
    println(s"\nThe word: ${game.guessedLetters}")
    if (game.mistakes >= maxMistakes) println("\nYou lost!")
    else if (game.guessedLetters == game.hiddenWord) println("\nYou won!")
    else nextStep(game)
  }

  def nextStep(game: Hangman): Unit = {
    val char = enterLetter
    if (game.hiddenWord.contains(char) && !game.guessedLetters.contains(char)) {
      println("Hit!")
      val guessedLetters: String = game.guessedLetters
        .zip(game.hiddenWord)
        .map {
          case ('*', c) if c == char => char
          case ('*', _)              => '*'
          case (c, _)                => c
        }
        .mkString("")
      checkStateAndGo(game.copy(guessedLetters = guessedLetters))
    } else {
      println(s"Missed, mistake ${game.mistakes + 1} out of $maxMistakes.")
      checkStateAndGo(game.copy(mistakes = game.mistakes + 1))
    }
  }

  def isLetter(input: String): Boolean = input.length == 1 && ('a' to 'z').contains(input.head)

  def enterLetter: Char = {
    val input = scala.io.StdIn.readLine
    if (isLetter(input)) input.head
    else {
      println("Error input. Enter lowercase letter.")
      enterLetter
    }
  }

}
