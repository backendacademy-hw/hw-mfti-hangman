object Main {
  def main(args: Array[String]): Unit = {
    val thesaurus = MyThesaurus.apply()
    Playground(thesaurus).start
  }
}
