import org.scalatest.flatspec.AnyFlatSpec

import java.io.{ByteArrayOutputStream, StringReader}

class HangmanSpec extends AnyFlatSpec {
  "Hangman" should "correct work with right answers" in {
    val testThesaurus = new Thesaurus {
      override def randomWord: String = "hello"
    }
    val testPlayground = Playground(testThesaurus)

    val input =
      """|h
         |e
         |l
         |o
         """.stripMargin
    val output =
      """|
         |The word: *****
         |Hit!
         |
         |The word: h****
         |Hit!
         |
         |The word: he***
         |Hit!
         |
         |The word: hell*
         |Hit!
         |
         |The word: hello
         |
         |You won!
         |""".stripMargin

    val in  = new StringReader(input)
    val out = new ByteArrayOutputStream()
    Console.withOut(out) {
      Console.withIn(in) {
        testPlayground.start
      }
    }

    assert(out.toString === output)
  }

  "Hangman" should "correct work with wrong answers" in {
    val testThesaurus = new Thesaurus {
      override def randomWord: String = "board"
    }
    val testPlayground = Playground(testThesaurus)

    val input =
      """|q
         |h
         |r
         |e
         |c
         |v
         """.stripMargin
    val output =
      """|
         |The word: *****
         |Missed, mistake 1 out of 5.
         |
         |The word: *****
         |Missed, mistake 2 out of 5.
         |
         |The word: *****
         |Hit!
         |
         |The word: ***r*
         |Missed, mistake 3 out of 5.
         |
         |The word: ***r*
         |Missed, mistake 4 out of 5.
         |
         |The word: ***r*
         |Missed, mistake 5 out of 5.
         |
         |The word: ***r*
         |
         |You lost!
         |""".stripMargin

    val in  = new StringReader(input)
    val out = new ByteArrayOutputStream()
    Console.withOut(out) {
      Console.withIn(in) {
        testPlayground.start
      }
    }

    assert(out.toString === output)
  }

  "Hangman" should "correct work with incorrect input" in {
    val testThesaurus = new Thesaurus {
      override def randomWord: String = "hello"
    }
    val testPlayground = Playground(testThesaurus)

    val input =
      """|h
         |e
         |1
         |*
         |/
         |l
         |o
         """.stripMargin
    val output =
      """|
         |The word: *****
         |Hit!
         |
         |The word: h****
         |Hit!
         |
         |The word: he***
         |Error input. Enter lowercase letter.
         |Error input. Enter lowercase letter.
         |Error input. Enter lowercase letter.
         |Hit!
         |
         |The word: hell*
         |Hit!
         |
         |The word: hello
         |
         |You won!
         |""".stripMargin

    val in  = new StringReader(input)
    val out = new ByteArrayOutputStream()
    Console.withOut(out) {
      Console.withIn(in) {
        testPlayground.start
      }
    }

    assert(out.toString === output)
  }
}
