import org.scalatest.flatspec.AnyFlatSpec

class ThesaurusSpec extends AnyFlatSpec {
  "Thesaurus " should "contain only lowercase words" in {
    val th    = MyThesaurus.apply()
    val regex = "([a-z]+)".r
    assert(
      th.words
        .map {
          case regex(_) => true
          case _        => false
        }
        .reduce(_ && _)
    )
  }

  "randomWord" should "return the word from thesaurus" in {
    val th = MyThesaurus.apply()
    assert(th.words.contains(th.randomWord))
  }

}
