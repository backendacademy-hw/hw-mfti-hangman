ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.4"

maintainer := "k.zubrilin@tinkoff.ru"

lazy val root = (project in file("."))
  .settings(
    name := "hw-mfti-hangman",
    coverageEnabled := true,
    coverageFailOnMinimum := true,
    coverageMinimumStmtTotal := 100,
    coverageMinimumBranchTotal := 100,
    coverageExcludedFiles := ".*\\/src\\/main\\/scala\\/Main",
  )



libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % Test

enablePlugins(JavaAppPackaging)
